﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {

	[SerializeField] private Subject s;
	
	private void OnTriggerEnter(Collider other) {
		if (other.GetComponent<Head>() != null) {
			if (other.GetComponent<Head>().AddSubject(s)) {
        		Destroy(gameObject);
			} else {
				
			}
       	}
	}
}
