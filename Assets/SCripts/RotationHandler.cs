﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandler : MonoBehaviour
{
	public float speed = 180;
	private bool inRotateModeP;

	public void Rotate(Vector3 direction) {
		StartCoroutine(RotateBy90Degree(direction));
	}

	IEnumerator RotateBy90Degree(Vector3 rotDir)
	{
		if (inRotateModeP)
		{
			yield break;
		}
		inRotateModeP = true;
	//	GetComponent<Renderer>().material.color = Color.red;

		float t = 0;
		Quaternion start = transform.rotation;
		var target = transform.rotation*Quaternion.Euler(rotDir*90);

		do
		{
			t += speed / 90f*Time.deltaTime;

			transform.rotation = Quaternion.Lerp(start, target, t);
			yield return null;
		} while (t<1f);

	//	GetComponent<Renderer>().material.color = Color.white;
		inRotateModeP = false;
	}
}