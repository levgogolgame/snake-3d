﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
	public GameObject PieObj;
	public Vector3 center;

	public Vector3 size;
	public Vector3 msize;

	private bool waitforfood = true;
	// Use this for initialization
	void Start () {
		SpawnFood();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.X))
			SpawnFood();
		
	}

	private IEnumerator WaitForFood()
	{
		yield return new WaitForSeconds(0.2f);
		waitforfood = true;
	}
	public void SpawnFood()
	{
		if (waitforfood)
		{
			//waitforfood = false;
			
			Vector3 position = center + new Vector3(Random.Range(msize.x,size.x),
				                   Random.Range(msize.y,size.y),
				                   Random.Range(msize.z,size.z));
			Instantiate(PieObj, position, Quaternion.identity);
//			                   new Vector3(Random.Range(-size.x / 6, size.x / 6),
//				                   Random.Range(-size.y / 6, size.y / 6),
//				                   Random.Range(-size.z / 6, size.z / 6));
		}

	//	WaitForFood();
		

	}
	
void OnDrawGizmosSelected()
{
	Gizmos.color = new Color(1, 0, 0,0.5f);
	Gizmos.DrawCube((center),size);
	Gizmos.color = new Color(0, 1, 0, 0.6f);

}
	
}