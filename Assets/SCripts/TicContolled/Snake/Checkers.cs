﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkers : MonoBehaviour {

	private Collider collider;
	
	void Start() {
		collider = GetComponent<Collider>();
	}

	public bool IsOnCube {
		get {
			Collider[] colliders =
				Physics.OverlapSphere(transform.position - transform.up + transform.forward * 1.2f, 0.1f);
			foreach (var collider1 in colliders) {
				if (collider1.GetComponent<Cube>() != null) {
					return true;
				}
			}

			return false;
		}
	}

	public bool IsWallForward {
		get {
			Collider[] colliders = Physics.OverlapSphere(transform.position + transform.forward, 0.1f);
			foreach (var collider1 in colliders) {
				if (collider1.GetComponent<Wall>() != null) {
					return true;
				}
			}

			return false;
		}
	}

	public bool CanUp {
		get {
			Collider[] colliders = Physics.OverlapSphere(transform.position + transform.forward, 0.1f);
			foreach (var collider1 in colliders) {
				if (collider1.GetComponent<Cube>() != null) {
					return true;
				}
			}

			return false;
		}
	}




}
