﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Move : MonoBehaviour, IControlled {
	
	[HideInInspector] public Vector3 direction = Vector3.forward;
	[SerializeField] private RotationHandler map;

	[SerializeField] private TicControler ticControler;
	private int countTic = 0;

	[SerializeField] private int ticOnMove = 5;
	public int TicOnMove {
		get { return ticOnMove;}
		set {
			if (value < 1) {
				ticOnMove = 1;
			} else {
				ticOnMove = value;
			}
		}
	}
	
	private Head head; // только для сущеностей с длинной

	private Checkers checkers;

	
	private void Start() {
		checkers = GetComponent<Checkers>();
		
		head = gameObject.GetComponent<Head>();
		
		ticControler.AddSubscriber(this);
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.K)) Debug.Log(transform.forward);
	}

	public void DiscretMove() {
		
		if (head != null) {
			if (head.Length > 0) {
				for (int i = head.Length - 1; i > 0; i--) {
					head.Tail[i].transform.position = head.Tail[i - 1].transform.position;
				}

				head.Tail[0].transform.position = transform.position;
			}
		}

		if (checkers.CanUp) {
			transform.Rotate(transform.right, -90, Space.World);
		//	map.Rotate(transform.right);
		}
		
		if (checkers.IsOnCube) {
			transform.position += transform.forward;			
		} else {
			transform.position += transform.forward - transform.up;
			transform.Rotate(transform.right, 90, Space.World);
		//	map.Rotate(-transform.right);
		}
	}
	
	public void Tic() {
		countTic++;
		if (countTic >= TicOnMove) {
			Rotate();
			if (checkers.IsWallForward) {
				head.Die();
			} else {
				DiscretMove();
			}
			countTic = 0;
		}
	}

	public void Rotate() {
		if (direction == Vector3.right) {
			transform.Rotate(transform.up, 90, Space.World);
		} else if (direction == Vector3.left) {
			transform.Rotate(transform.up, -90, Space.World);
		}
		direction = Vector3.forward;
	}


	public void UpSpeed(int incraseSpeed, float howLong) {
		TicOnMove -= incraseSpeed;
		if (howLong > 0) {
			StartCoroutine(DownSpeed(incraseSpeed, howLong));
		} else {
			//навсегда
		}
	}

	public IEnumerator DownSpeed(int downSpeed, float howLong) {
			yield return new WaitForSeconds(howLong);

			TicOnMove += downSpeed;
			
	}
}
