﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Head : MonoBehaviour {

	private GameObject parrentTail;
	
	[HideInInspector] public List<Tail> Tail;
	[SerializeField] private GameObject tailPrefab;
	[SerializeField] private MiniInventory inventory;
	
	private int length = 0;
	public int Length {
		get { return length;}
		set {
			if (length >= value) {
				for (int i = length - 1; i >= value; i--) {
					if (i >= 0) {
						Tail[i].gameObject.SetActive(false);
					}
				}

				length = value > 0 ? value : 0; 
			} else {
				StartCoroutine(BuildUp(value - length));
			}
		}
	}


	private void Start() {
		Tail = new List<Tail>();
		
		parrentTail = GameObject.Find("Tail"); 

	}

	//сделать активным элемент хвоста или создать новый
	private IEnumerator BuildUp(int j) {
		for (int i = 0; i < j; i++) {
			if (Tail.Count > length) {
				Tail[length].gameObject.SetActive(true);
				Tail[length].gameObject.transform.position =
					length == 0 ? -transform.forward : -Tail[length - 1].transform.forward;
			} else if (Tail.Count == length) {
				Tail.Add(Instantiate(tailPrefab, Tail.Count == 0 ? -transform.forward : -Tail[Tail.Count - 1].transform.forward,
					Quaternion.identity).GetComponent<Tail>());
				Tail[Tail.Count - 1].gameObject.transform.SetParent(parrentTail.transform);		
			} else {
				Debug.Log("Error with Length");
			}
			
			length++;
			yield return new WaitForFixedUpdate();
		}
	}

	public void Die() {
		
		Debug.Log("Die");
	}

	public bool AddSubject(Subject subjectName) {
		return inventory.addSubject(subjectName);
	}

	public bool OpenStain(Subject key) {
		if (inventory.haveSubject(key.name)) {
			inventory.removeSubject(key);
			return true;
		}

		return false;
	}
}
