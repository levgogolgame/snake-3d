﻿using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Environments;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class InputForSnake : MonoBehaviour {

	[SerializeField] private Head head;
	[SerializeField] private Move move;

	private void Start() {
		head = gameObject.GetComponent<Head>();
	}

	void Update ()
	{		
		if (Input.GetKeyDown(KeyCode.E)) {
			move.direction = Vector3.right;
		}
		else if (Input.GetKeyDown(KeyCode.Q)) {
			move.direction = Vector3.left;
		}
	}

	
}
