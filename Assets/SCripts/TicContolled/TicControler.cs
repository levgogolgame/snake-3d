﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;


public class TicControler : MonoBehaviour {

	public delegate void Tic();
	public event Tic Count;

	private float timer = 0.01f;
	public float delay = 0.01f;

	private void Update() {
		if (timer <= 0) {
			timer = delay;
			if (Count != null) Count();
		}
		timer -= Time.deltaTime;
	}

	public void AddSubscriber(IControlled controlled) {
		Count += controlled.Tic;
	}

	public void RemoveSubscriber(IControlled controlled) {
		Count -= controlled.Tic;
	}

}
