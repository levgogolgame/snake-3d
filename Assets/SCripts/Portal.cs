﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

	[SerializeField] private Portal exit;
	[HideInInspector] public bool isExit;

	private void OnTriggerEnter(Collider other) {
		if (!isExit) {
			if (other.GetComponent<Head>() != null) {
				exit.isExit = true;
				other.transform.position = exit.transform.position;
				other.transform.rotation = exit.transform.rotation;
			}
		}
	}

	private void OnTriggerExit(Collider other) {
		isExit = false;
	}
}

