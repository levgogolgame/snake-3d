﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsHandler : MonoBehaviour {

	private Text text;
	private int points;

	public int Points {
		get { return points; }
		set {
			points = value;
			text.text = "Points" + points;
		}
	}

	void Start () {
		text = GetComponent<Text>();
	}

	
}
