﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

	private bool isPause = false;
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (isPause) {
				Time.timeScale = 1;
				isPause = false;
			} else {
				Time.timeScale = 0;
				isPause = true;
			}
		} 
	}
}
