﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsHandler : MonoBehaviour {

	private Text text;

	private int coins;
	public int Coins {
		get { return coins;}
		set {
			coins = value;
			text.text = "Coins " + coins;
		}
	}
	
	void Start () {
		text = GetComponent<Text>();
	}
	
}
