﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer: MonoBehaviour {

	[SerializeField] private float timer;
	private Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		timer += Time.fixedDeltaTime;
		
		text.text = timer.ToTime();
	}
}

public static class ForExtension
{
	//убейте меня
	public static string ToTime(this float inSeconds) {
		int a = (int) inSeconds;

		string result; 
		
		int seconds = a % 60;
		result = seconds / 10 == 0 ? "0" + seconds : seconds.ToString();
		
		int minuts;
		int hours;
		
		if (a / 60 >= 1) {
			minuts = a  % (60 * 60) / 60;
			result = (minuts / 10 == 0 ? "0" + minuts : minuts.ToString()) + ":" + result;
		} else {
			result = "00:" + result;
		}

		if (a / (60 * 60) >= 1) {
			hours = a / 60 / 60;
			result = hours.ToString() + ":" + result;
		}

		return result;
	}
} 
