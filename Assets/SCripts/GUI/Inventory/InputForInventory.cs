﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputForInventory : MonoBehaviour {

	[SerializeField] private MiniInventory inventory;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			inventory.ActiveElement++;

		} else if (Input.GetKeyDown(KeyCode.LeftArrow)){
			inventory.ActiveElement--;
		}
	}
}
