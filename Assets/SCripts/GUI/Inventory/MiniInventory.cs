﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boo.Lang.Environments;
using UnityEngine;
using UnityEngine.UI;



public class MiniInventory : MonoBehaviour {
	
	private Subject[] inventory = new Subject[4];

	[SerializeField] private Image[] images = new Image[4];
	[SerializeField] private Image[] activeImages = new Image[4];
	
	private int activeElement = 4;   
	public int ActiveElement {			
		get { return activeElement;}
		set {	
			//off
			if (activeElement != 4) {
				activeImages[activeElement].gameObject.SetActive(false);
			}
			//on
			bool onRight = value > activeElement;
			
			if (value < 0) {
				activeElement = 4;
			} else if (value > 4) {
				activeElement = 0;
			} else {
				activeElement = value;
			}
			
			if (activeElement != 4) {
				if (images[activeElement].gameObject.activeSelf) {
					activeImages[activeElement].gameObject.SetActive(true);
				} else {
					if (onRight) {
						ActiveElement++;
					} else {
						ActiveElement--;
					}
				}
			}
		}
	}

	public bool addSubject(Subject subjectName) {	//TODO stack subjects
		for (int i = 0; i <= 3; i++) {
			if (inventory[i] == null) {
				inventory[i] = subjectName;
				images[i].gameObject.SetActive(true);
				images[i].sprite = subjectName.sprite;
				return true;
			}
		}
		
		Debug.Log("inventory is full");
		return false;
	}

	public void removeSubject(Subject subjectName) {
		for (int i = 0; i <= 3; i++) {
			if (inventory[i] == subjectName) {
				inventory[i] = null;
				images[i].gameObject.SetActive(false);
				return;
			}
		}	
		Debug.Log("no subject to remove");
	}

	public bool haveSubject(string subject) {
		for (int i = 0; i <= 3; i++) {
			if (inventory[i] != null) {
				if (inventory[i].name == subject) {
					return true;
				}
			}
		}
		return false;
	}

}
