﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New SubjectData", menuName = "Subject Data", order = 51)]
public class Subject : ScriptableObject {
	public string name;
	public Sprite sprite;
	public int count;
}
