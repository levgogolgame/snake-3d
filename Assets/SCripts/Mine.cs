﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {

		Head head = other.gameObject.GetComponent<Head>() ;
		if (head != null) {
			head.Die();
		}
	}
}
