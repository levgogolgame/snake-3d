﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{

	public float speed = 5f;
	[SerializeField] private GameObject _target;
	private UnityEngine.Camera cam;
	
	[SerializeField] private Vector3 offset;
	//for rotation
	private float xspeed = 150f;
	private float yspeed = 150f;
	private float xangle = 0f;
	private float yangle = 0f;
	private float distance = 5f;
	
	//for zooming and scrolling
	private Vector3 lastScrollingPosition;
	public float scrollingmove = 20f;
	private float[] zoomDistance = {20f, 80f};
	[SerializeField] private float[] distanceX =  {0f, 0f};
	[SerializeField] private float[] distanceZ =  {0f, 0f};

	void Start()
	{
		offset = GetComponent<Camera>().offset;
		cam = GetComponent<UnityEngine.Camera>();
		var angles = transform.eulerAngles; 
		xangle = angles.y; 
		yangle = angles.x; 
	}

	private void LateUpdate()
	{
		if (_target)
		{
			var rotation = Quaternion.Euler(yangle, xangle, 0);
			transform.rotation = rotation;
			var position = rotation * Vector3.Lerp(offset, 
				               offset, -distance) + 
			               _target.transform.position;
			transform.position = position;
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			lastScrollingPosition = Input.mousePosition;
		}
		else if (Input.GetMouseButton(0))
		{
			ScrollCamera(Input.mousePosition);
		}

		float scroll = Input.GetAxis("Mouse ScrollWheel");
		ZoomCamera(scroll, scrollingmove);


	}

	public void ScrollCamera(Vector3 newScrollPosition)
	{
		Vector3 offset = cam.ScreenToViewportPoint(lastScrollingPosition - newScrollPosition);
		Vector3 moving=new Vector3(offset.x*scrollingmove,0,offset.y*scrollingmove);
		transform.Translate(moving,Space.Self);
		Vector3 position = transform.position;
		position.x = Mathf.Clamp(transform.position.x, distanceX[0], distanceX[1]);
		position.z = Mathf.Clamp(transform.position.z, distanceZ[0], distanceZ[1]);
		_target.transform.Translate(moving,Space.Self);
		lastScrollingPosition = newScrollPosition;
		
	}
	

	void ZoomCamera(float offset, float speed) {
		if (offset == 0) {
			return;
		}
    
		cam.fieldOfView = Mathf.Clamp(cam.fieldOfView - (offset * speed), zoomDistance[0], zoomDistance[1]);
	}
	
}