﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : Edible {

	[SerializeField] private int incraseSpeed;
	[SerializeField] private float howLong;
	
	private void OnTriggerEnter(Collider other) {
		var move = other.gameObject.GetComponent<Move>();
		
		if (move != null) {
			move.UpSpeed(incraseSpeed, howLong);
		}
		Destroy(gameObject);
	}
}
