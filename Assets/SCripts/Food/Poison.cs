﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Edible {

	public int points = -400;
	private PointsHandler ph;
	
	void Start () {
		ph = GameObject.FindGameObjectWithTag("PointHandler").GetComponent<PointsHandler>();
	}

	private void OnTriggerEnter(Collider other) {
		if (other.GetComponent<Head>() != null) {
			ph.Points += points;
			other.GetComponent<Head>().Length -= 4;
			Destroy(gameObject);
		}
		
	}
}
