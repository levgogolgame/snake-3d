﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Edible {

	private int coins = 1;
	private CoinsHandler ch;
	
	void Start () {
		ch = GameObject.FindGameObjectWithTag("CoinsHandler").GetComponent<CoinsHandler>();
	}
	
	private void OnTriggerEnter(Collider other) {
		if (other.GetComponent<Head>() != null) {
			ch.Coins += coins;
			Destroy(gameObject);
		}
		
	}
}
