﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : Edible {

	public int points = 100;
	private PointsHandler _ph;
	
	void Start () {
		_ph = GameObject.FindGameObjectWithTag("PointHandler").GetComponent<PointsHandler>();
	}

	private void OnTriggerEnter(Collider other) {
		if (other.GetComponent<Head>() != null) {
			_ph.Points += points;
			other.GetComponent<Head>().Length++;
			Destroy(gameObject);
		}
		
	}
}
