﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stain : MonoBehaviour {

	[SerializeField] private Subject subject;

	private void OnTriggerEnter(Collider other) {
		

		Head head = other.gameObject.GetComponent<Head>() ;
		if (head != null) {
			if (head.OpenStain(subject)) {
				Destroy(gameObject);
			} else {
				head.Die();
			}
		}
	}
}
